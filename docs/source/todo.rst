.. nodoctest

TODO
====

#. Create a proper extension module for `crunch.c <zetalib/crunch.c>`_.
#. Improve the installation instructions. Possibly split to install guide and
   usage guide.
#. Make Zeta into an experimental SageMath package.
#. Add doctests to all modules, and add them to the documentation.
#. Update ``CHANGES`` to use ReST.
#. Use Sage LaurentPolynomialRing?
#. Use the Sage LattE interface, or consider an update to it by adding
   maple.cpp as sage.cpp.  See :trac:`18232`, :trac:`18211`, :trac:`22067`,
   :trac:`22099`, :trac:`22109`, :trac:`22066`, :trac:`22111`. 
#. Add topzetas.txt to the static documentation.
#. Add GitLab Continuous integration.
#. Add a Jupyter demo notebook with Binder support (https://opendreamkit.org/2018/07/23/live-online-slides-with-sagemath-jupyter-rise-binder/).
#. Consider https://github.com/sagemath/sage-package for stand-alone docs that do not require having Sage installed. This is useful for https://readthedocs.org.
