.. nodoctest

Algebra
=======

.. automodule:: zetalib.algebra
   :members:
   :undoc-members:
   :show-inheritance:
